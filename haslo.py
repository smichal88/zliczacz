#!/usr/bin/python
#-*- coding: utf-8 -*-

from PyKDE4.kdeui import KWallet
from PyQt4 import QtGui
import getpass


def open_wallet():
    app = QtGui.QApplication([])
    wallet = KWallet.Wallet.openWallet(
                 KWallet.Wallet.LocalWallet(), 0)

    if not wallet.hasFolder('Godziny'):
        nazwa = raw_input("Wprowadź adres mail:\n")
        haslo = getpass.getpass("Wprowadź hasło:\n")
        wallet.createFolder('Godziny')
        wallet.setFolder('Godziny')
        wallet.writePassword('email', nazwa)
        wallet.writePassword('pass', haslo)
    else:
        wallet.setFolder('Godziny')
    return wallet


def get_password(wallet):
    key, qstr_password = wallet.readPassword('pass')

    # converting the password from PyQt4.QtCore.QString to str
    return str(qstr_password)


def get_email(wallet):
    key, qstr_password = wallet.readPassword('email')
    return str(qstr_password)


if __name__ == "__main__":
    wall = open_wallet()
