#!/usr/bin/python
#-*- coding: utf-8 -*-

try:
    from xml.etree import ElementTree # for Python 2.5 users
except ImportError:
    from elementtree import ElementTree
import gdata.calendar.service
import gdata.service
import gdata.calendar
from datetime import date, timedelta, datetime
import time
import haslo


def GetAuthCal():
    calendar_service = gdata.calendar.service.CalendarService()
    wal = haslo.open_wallet()

    calendar_service.email = haslo.get_email(wal)
    calendar_service.password = haslo.get_password(wal)
    calendar_service.source = "Zliczacz godzin"
    calendar_service.ProgrammaticLogin()
    return calendar_service

def PrintOwnCalendars(calendar_service):
    feed = calendar_service.GetOwnCalendarsFeed()
    print feed.title.text
    for i, a_calendar in enumerate(feed.entry):
        print '\t%s. %s' % (i, a_calendar.title.text,)

def DateRangeQuery(calendar_service, start_date=time.strftime("%Y-%m-%d"), end_date=time.strftime("%Y-%m-%d")):
    print 'Date range query for events on Primary Calendar: %s to %s' % (start_date, end_date,)
    query = gdata.calendar.service.CalendarEventQuery('default', 'private', 'full')
    query.start_min = start_date
    query.start_max = end_date
    feed = calendar_service.CalendarQuery(query)
    sumS = timedelta()
    for i, an_event in enumerate(feed.entry):
        print '\t%s. %s' % (i, an_event.title.text,)
        for a_when in an_event.when:
            print '\t\tStart time: %s' % (a_when.start_time,)
            print '\t\tEnd time:   %s' % (a_when.end_time,)
            start = datetime.strptime(a_when.start_time[:19], "%Y-%m-%dT%H:%M:%S")
            end = datetime.strptime(a_when.end_time[:19], "%Y-%m-%dT%H:%M:%S")
            timeS = (end - start)
            sumS += timeS
            print '\t\tTime: %s' % timeS
    s = sumS.total_seconds()
    m = (s % 3600)//60
    h = s //3600
    print '\tSum: %s:%s' % (int(h), m)




def main():
    cal = GetAuthCal()
    PrintOwnCalendars(cal)
    firstDayOfWeek = date.today() - timedelta(days = date.weekday(date.today()))
    lastDayOfWeek = firstDayOfWeek + timedelta(days = 6)
    DateRangeQuery(cal, time.strftime("%Y-%m-%d", firstDayOfWeek.timetuple()), time.strftime("%Y-%m-%d", lastDayOfWeek.timetuple()))

if __name__ == "__main__":
    main()
